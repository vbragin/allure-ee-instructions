# AllureEE instructions for newbees or for me.

## GitLab integration.
User story: As an automation engineer i want to execute my gitlab-ci from AllueEE. 

User story: As a test engineer i want to see in AllureEE allure-results from each gitlab execution.

#### STEP 1. Configure ALLURE_TOKEN.
1. Open AllureEE
2. Go to profile
3. Create token with name, for example `GITLAB`
4. Copy this token
5. Save as Gitlab CI variable ALLURE_TOKEN

#### STEP 2. Configure ALLURE_ENDPOINT.
1. Copy address of AllureEE Server, for example http://10.10.10.10 or with port if it has port
2. Save as Gitlab CI variable ALLURE_ENDPOINT

#### STEP 3. Confligure ALLURE_PROJECT_ID.
1. Create project in AllureEE
2. Copy project id
3. Save as Gitlab CI variable ALLURE_PROJECT_ID
