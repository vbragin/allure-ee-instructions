#!/bin/sh
export ALLURE_LAUNCH_NAME='Launch from gitlab ci'
export RESULTS_PATH='build/allure-results'
echo "${JAVA_HOME}"
wget https://dl.bintray.com/qameta/maven-unstable/io/qameta/allure/allure-ee-commandline/3.18.2/allure-ee-commandline-3.18.2.zip
unzip allure-ee-commandline-3.18.2.zip

if [ -z "${AS_LAUNCH_ID}" ] || [ -z "${AS_EXECUTION_ID}" ]; then
  echo "Upload to new launch..."
  ./allure-ee-3.18.2/bin/allure-ee --verbose upload "${RESULTS_PATH}" \
      --token "${ALLURE_TOKEN}" \
      --endpoint "${ALLURE_ENDPOINT}" \
      --projectId "${ALLURE_PROJECT_ID}" \
      --launchName "${ALLURE_LAUNCH_NAME}" \
      --executionName "${CI_PIPELINE_ID}" \
      --executionUrl "${CI_PIPELINE_URL}" \
      --jobName "${CI_PROJECT_NAME}" \
      --jobUrl "${CI_PROJECT_URL}/pipelines" \
      --jobExternalId "${CI_PROJECT_ID}" \
      --executor "gitlab" \
      --timeout 300
else
  echo "Upload to existing launch ${AS_EXECUTION_ID}..."
  ./allure-ee-3.18.2/bin/allure-ee --verbose upload "${RESULTS_PATH}" \
      --token "${ALLURE_TOKEN}" \
      --endpoint "${ALLURE_ENDPOINT}" \
      --projectId "${ALLURE_PROJECT_ID}" \
      --launchId "${AS_LAUNCH_ID}" \
      --executionId "${AS_EXECUTION_ID}" \
      --executionName "${CI_PIPELINE_ID}" \
      --executionUrl "${CI_PIPELINE_URL}" \
      --executor "gitlab" \
      --timeout 300
fi
