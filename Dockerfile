FROM openjdk:8
FROM python:3.7-alpine
ENV JAVA_HOME /usr/local/openjdk-8
COPY requirements.txt /app/requirements.txt

WORKDIR /app
RUN python3 -c 'import sys; f = open("/usr/local/lib/python3.7/site-packages/_manylinux.py", "w"); f.write("manylinux1_compatible = True"); f.close()'
RUN apk --no-cache add curl gcc libc-dev
RUN pip install --no-cache-dir -r requirements.txt